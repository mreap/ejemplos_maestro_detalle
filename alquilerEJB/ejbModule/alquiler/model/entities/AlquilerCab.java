package alquiler.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the alquiler_cab database table.
 * 
 */
@Entity
@Table(name="alquiler_cab")
@NamedQuery(name="AlquilerCab.findAll", query="SELECT a FROM AlquilerCab a")
public class AlquilerCab implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_alquiler_cab", unique=true, nullable=false)
	private Integer idAlquilerCab;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date fecha;

	@Column(name="valor_total", nullable=false, precision=7, scale=2)
	private BigDecimal valorTotal;

	//bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name="cedula_cliente", nullable=false)
	private Cliente cliente;

	//bi-directional many-to-one association to AlquilerDet
	@OneToMany(mappedBy="alquilerCab",cascade = CascadeType.ALL)
	private List<AlquilerDet> alquilerDets;

	public AlquilerCab() {
	}

	public Integer getIdAlquilerCab() {
		return this.idAlquilerCab;
	}

	public void setIdAlquilerCab(Integer idAlquilerCab) {
		this.idAlquilerCab = idAlquilerCab;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getValorTotal() {
		return this.valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<AlquilerDet> getAlquilerDets() {
		return this.alquilerDets;
	}

	public void setAlquilerDets(List<AlquilerDet> alquilerDets) {
		this.alquilerDets = alquilerDets;
	}

	public AlquilerDet addAlquilerDet(AlquilerDet alquilerDet) {
		getAlquilerDets().add(alquilerDet);
		alquilerDet.setAlquilerCab(this);

		return alquilerDet;
	}

	public AlquilerDet removeAlquilerDet(AlquilerDet alquilerDet) {
		getAlquilerDets().remove(alquilerDet);
		alquilerDet.setAlquilerCab(null);

		return alquilerDet;
	}

}