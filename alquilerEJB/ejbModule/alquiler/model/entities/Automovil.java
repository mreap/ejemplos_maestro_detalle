package alquiler.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the automovil database table.
 * 
 */
@Entity
@Table(name="automovil")
@NamedQuery(name="Automovil.findAll", query="SELECT a FROM Automovil a")
public class Automovil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique=true, nullable=false, length=10)
	private String placa;

	private Boolean alquilado;

	private Integer anio;

	@Column(length=30)
	private String color;

	@Column(name="costo_dia", nullable=false, precision=7, scale=2)
	private BigDecimal costoDia;

	private Integer kilometraje;

	//bi-directional many-to-one association to AlquilerDet
	@OneToMany(mappedBy="automovil")
	private List<AlquilerDet> alquilerDets;

	//bi-directional many-to-one association to Marca
	@ManyToOne
	@JoinColumn(name="id_marca")
	private Marca marca;

	public Automovil() {
	}

	public String getPlaca() {
		return this.placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Boolean getAlquilado() {
		return this.alquilado;
	}

	public void setAlquilado(Boolean alquilado) {
		this.alquilado = alquilado;
	}

	public Integer getAnio() {
		return this.anio;
	}

	public void setAnio(Integer anio) {
		this.anio = anio;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public BigDecimal getCostoDia() {
		return this.costoDia;
	}

	public void setCostoDia(BigDecimal costoDia) {
		this.costoDia = costoDia;
	}

	public Integer getKilometraje() {
		return this.kilometraje;
	}

	public void setKilometraje(Integer kilometraje) {
		this.kilometraje = kilometraje;
	}

	public List<AlquilerDet> getAlquilerDets() {
		return this.alquilerDets;
	}

	public void setAlquilerDets(List<AlquilerDet> alquilerDets) {
		this.alquilerDets = alquilerDets;
	}

	public AlquilerDet addAlquilerDet(AlquilerDet alquilerDet) {
		getAlquilerDets().add(alquilerDet);
		alquilerDet.setAutomovil(this);

		return alquilerDet;
	}

	public AlquilerDet removeAlquilerDet(AlquilerDet alquilerDet) {
		getAlquilerDets().remove(alquilerDet);
		alquilerDet.setAutomovil(null);

		return alquilerDet;
	}

	public Marca getMarca() {
		return this.marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

}