package alquiler.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the marca database table.
 * 
 */
@Entity
@Table(name="marca")
@NamedQuery(name="Marca.findAll", query="SELECT m FROM Marca m")
public class Marca implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_marca", unique=true, nullable=false)
	private Integer idMarca;

	@Column(name="nombre_marca", nullable=false, length=100)
	private String nombreMarca;

	//bi-directional many-to-one association to Automovil
	@OneToMany(mappedBy="marca")
	private List<Automovil> automovils;

	public Marca() {
	}

	public Integer getIdMarca() {
		return this.idMarca;
	}

	public void setIdMarca(Integer idMarca) {
		this.idMarca = idMarca;
	}

	public String getNombreMarca() {
		return this.nombreMarca;
	}

	public void setNombreMarca(String nombreMarca) {
		this.nombreMarca = nombreMarca;
	}

	public List<Automovil> getAutomovils() {
		return this.automovils;
	}

	public void setAutomovils(List<Automovil> automovils) {
		this.automovils = automovils;
	}

	public Automovil addAutomovil(Automovil automovil) {
		getAutomovils().add(automovil);
		automovil.setMarca(this);

		return automovil;
	}

	public Automovil removeAutomovil(Automovil automovil) {
		getAutomovils().remove(automovil);
		automovil.setMarca(null);

		return automovil;
	}

}