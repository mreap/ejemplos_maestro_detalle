package alquiler.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the cliente database table.
 * 
 */
@Entity
@Table(name="cliente")
@NamedQuery(name="Cliente.findAll", query="SELECT c FROM Cliente c")
public class Cliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cedula_cliente", unique=true, nullable=false, length=10)
	private String cedulaCliente;

	@Column(nullable=false, length=100)
	private String direccion;

	@Column(nullable=false, length=50)
	private String nombres;

	@Column(nullable=false, length=15)
	private String telefono;

	//bi-directional many-to-one association to AlquilerCab
	@OneToMany(mappedBy="cliente")
	private List<AlquilerCab> alquilerCabs;

	public Cliente() {
	}

	public String getCedulaCliente() {
		return this.cedulaCliente;
	}

	public void setCedulaCliente(String cedulaCliente) {
		this.cedulaCliente = cedulaCliente;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<AlquilerCab> getAlquilerCabs() {
		return this.alquilerCabs;
	}

	public void setAlquilerCabs(List<AlquilerCab> alquilerCabs) {
		this.alquilerCabs = alquilerCabs;
	}

	public AlquilerCab addAlquilerCab(AlquilerCab alquilerCab) {
		getAlquilerCabs().add(alquilerCab);
		alquilerCab.setCliente(this);

		return alquilerCab;
	}

	public AlquilerCab removeAlquilerCab(AlquilerCab alquilerCab) {
		getAlquilerCabs().remove(alquilerCab);
		alquilerCab.setCliente(null);

		return alquilerCab;
	}

}