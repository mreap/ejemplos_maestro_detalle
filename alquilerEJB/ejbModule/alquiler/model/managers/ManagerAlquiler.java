package alquiler.model.managers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import alquiler.model.dtos.DTODetalleAlquiler;
import alquiler.model.entities.AlquilerCab;
import alquiler.model.entities.AlquilerDet;
import alquiler.model.entities.Automovil;
import alquiler.model.entities.Cliente;
import alquiler.model.entities.Marca;

/**
 * Session Bean implementation class ManagerAlquiler
 */
@Stateless
@LocalBean
public class ManagerAlquiler {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public ManagerAlquiler() {
        
    }
    
    public List<Marca> findAllMarca(){
    	TypedQuery<Marca> q = em.createQuery("select m from Marca m order by m.nombreMarca", Marca.class);
    	return q.getResultList();
    }
    
    public List<Cliente> findAllCliente(){
    	TypedQuery<Cliente> q=em.createQuery("select c from Cliente c order by c.nombres", Cliente.class);
    	return q.getResultList();
    }
    
    public List<Automovil> findAllAutomovil(){
    	TypedQuery<Automovil> q=em.createQuery("select a from Automovil a order by a.placa", Automovil.class);
    	return q.getResultList();
    }
    
    public Automovil cambiarEstadoAlquiler(Automovil automovil) {
    	//Automovil a=em.find(Automovil.class, placa);
    	automovil.setAlquilado(!automovil.getAlquilado());
    	em.merge(automovil);
    	return automovil;
    }
    public Automovil actualizarAutomovil(Automovil automovilEdicion,int idMarcaEdicion) {
    	Automovil a=em.find(Automovil.class, automovilEdicion.getPlaca());
    	a.setAnio(automovilEdicion.getAnio());
    	a.setColor(automovilEdicion.getColor());
    	a.setKilometraje(automovilEdicion.getKilometraje());
    	a.setCostoDia(automovilEdicion.getCostoDia());
    	Marca m=em.find(Marca.class, idMarcaEdicion);
    	a.setMarca(m);
    	em.merge(a);
    	return a;
    }
    
    public void alquilerSimpleAutomovil(String placa, String cedulaCliente) throws Exception {
    	Automovil a=em.find(Automovil.class, placa);
    	Cliente c=em.find(Cliente.class, cedulaCliente);
    	
    	//comprobar:
    	if(a.getAlquilado())
    		throw new Exception("El automovil ya está alquilado.");
    	
    	//crear la cabecera y detalle del alquiler:
    	AlquilerCab alquilerCab=new AlquilerCab();
    	alquilerCab.setCliente(c);
    	alquilerCab.setFecha(new Date());
    	double valorTotal=1*a.getCostoDia().doubleValue();
    	alquilerCab.setValorTotal(new BigDecimal(valorTotal));
    	
    	AlquilerDet alquilerDet=new AlquilerDet();
    	alquilerDet.setAlquilerCab(alquilerCab);
    	alquilerDet.setAutomovil(a);
    	alquilerDet.setNumeroDias(1);
    	alquilerDet.setSubtotal(new BigDecimal(valorTotal));
    	
    	alquilerCab.setAlquilerDets(new ArrayList<AlquilerDet>());
    	alquilerCab.addAlquilerDet(alquilerDet);
    	
    	em.persist(alquilerCab);
    	
    	a.setAlquilado(true);
    	em.merge(a);
    	
    }
    
    public DTODetalleAlquiler crearDetalleAlquiler(String placa,int numeroDias) {
    	Automovil a=em.find(Automovil.class, placa);
    	
    	DTODetalleAlquiler detalle=new DTODetalleAlquiler();
    	detalle.setPlaca(placa);
    	detalle.setCostoDia(a.getCostoDia().doubleValue());
    	detalle.setDescripcion(a.getPlaca()+" "+a.getColor()+" "+a.getAnio()+" "+a.getKilometraje());
    	detalle.setNumeroDias(numeroDias);
    	detalle.setSubtotal(a.getCostoDia().doubleValue()*numeroDias);
    	
    	return detalle;
    }
    
    public double totalDetalleAlquiler(List<DTODetalleAlquiler> listaDetalleAlquiler) {
    	double total=0;
    	for(DTODetalleAlquiler d:listaDetalleAlquiler)
    		total+=d.getSubtotal();
    	return total;
    }
    
    public void alquilerMultimpleAutomovil(List<DTODetalleAlquiler> listaDetalleAlquiler,String cedulaCliente) {
    	Cliente cliente=em.find(Cliente.class, cedulaCliente);
    	AlquilerCab acab=new AlquilerCab();
    	acab.setCliente(cliente);
    	acab.setFecha(new Date());
    	acab.setValorTotal(new BigDecimal(totalDetalleAlquiler(listaDetalleAlquiler)));
    	
    	List<AlquilerDet> listaDetalle=new ArrayList<AlquilerDet>();
    	acab.setAlquilerDets(listaDetalle);
    	
    	for(DTODetalleAlquiler det:listaDetalleAlquiler) {
    		AlquilerDet alqDet=new AlquilerDet();
    		alqDet.setAlquilerCab(acab);
    		Automovil automovil=em.find(Automovil.class, det.getPlaca());
    		alqDet.setAutomovil(automovil);
    		alqDet.setNumeroDias(det.getNumeroDias());
    		alqDet.setSubtotal(new BigDecimal(det.getSubtotal()));
    		listaDetalle.add(alqDet);
    	}
    	
    	em.persist(acab);
    }

}
