CREATE TABLE marca
(
   id_marca serial, 
   nombre_marca character varying(100) NOT NULL, 
   CONSTRAINT pk_marca PRIMARY KEY (id_marca)
);

CREATE TABLE automovil
(
   placa varchar(10) primary key,
   anio integer,
   color varchar(30),
   id_marca integer references marca(id_marca),
   alquilado boolean,
   kilometraje integer,
   costo_dia numeric(7,2) not null
);

insert into marca(nombre_marca) values('MAZDA');
insert into marca(nombre_marca) values('FORD');
insert into marca(nombre_marca) values('TOYOTA');

insert into automovil values('IPJ2345',2015,'BLANCO',2,false,30000,70.45);
insert into automovil values('PXQ9854',2014,'NEGRO',3,true,45600,65.14);
insert into automovil values('LJB2398',2020,'ROJO',3,false,5000,85.20);
insert into automovil values('GEK7658',2018,'GRIS',1,false,15500,78.30);

CREATE TABLE cliente
(
   cedula_cliente varchar(10) primary key,
   nombres varchar(50) not null,
   direccion varchar(100) not null,
   telefono varchar(15) not null
);

insert into cliente values('100','Luis Andrade','Av. Teodoro Gómez - Ibarra','099100');
insert into cliente values('101','Marta Garrido','Sucre 3 45 - Ibarra','099101');
insert into cliente values('102','Susana Jácome','Atahualpa y 10 de Agosto - Otavalo','099102');
insert into cliente values('103','Sergio Rodríguez','Av. Bolívar 234 - Cotacachi','099103');
insert into cliente values('104','Carla Jurado','Bolívar 893 - Atuntaqui','099104');

CREATE TABLE alquiler_cab
(
  id_alquiler_cab serial primary key,
  fecha date not null,
  cedula_cliente varchar(10) not null references cliente(cedula_cliente),
  valor_total numeric(7,2) not null
);

CREATE TABLE alquiler_det
(
   id_alquiler_det serial primary key,
   id_alquiler_cab integer not null references alquiler_cab(id_alquiler_cab),
   placa varchar(10) not null references automovil(placa),
   numero_dias smallint not null,
   subtotal numeric(7,2) not null
);

