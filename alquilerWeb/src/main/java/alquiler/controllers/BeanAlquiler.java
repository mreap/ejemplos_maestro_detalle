package alquiler.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import alquiler.controllers.util.JSFUtil;
import alquiler.model.dtos.DTODetalleAlquiler;
import alquiler.model.entities.Automovil;
import alquiler.model.entities.Cliente;
import alquiler.model.entities.Marca;
import alquiler.model.managers.ManagerAlquiler;

@Named
@SessionScoped
public class BeanAlquiler implements Serializable {
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerAlquiler managerAlquiler;
	
	private List<Automovil> listaAutomoviles;
	private List<Marca> listaMarcas;
	private List<Cliente> listaClientes;
	private List<DTODetalleAlquiler> listaDetalleAlquiler;
	
	private Automovil automovilEdicion;
	private int idMarcaEdicion;
	private String cedulaClienteSeleccionado;
	private String placaAutomovilSeleccionado;
	private int numeroDias;
	private double total;

	public BeanAlquiler() {
		
	}

	@PostConstruct
	public void inicializar() {
		listaMarcas=managerAlquiler.findAllMarca();
		listaAutomoviles=managerAlquiler.findAllAutomovil();
		listaClientes=managerAlquiler.findAllCliente();
		listaDetalleAlquiler=new ArrayList<DTODetalleAlquiler>();
	}
	
	public void actionListenerCambiarEstadoAlquiler(Automovil automovil) {
		managerAlquiler.cambiarEstadoAlquiler(automovil);
		JSFUtil.crearMensajeInfo("Estado cambiado");
	}
	
	public void actionListenerSeleccionarAutomovilEdicion(Automovil automovil) {
		automovilEdicion=automovil;
		idMarcaEdicion=automovil.getMarca().getIdMarca();
	}
	
	public void actionListenerActualizarAutomovil() {
		managerAlquiler.actualizarAutomovil(automovilEdicion, idMarcaEdicion);
		listaAutomoviles=managerAlquiler.findAllAutomovil();
		JSFUtil.crearMensajeInfo("Automóvil actualizado.");
	}
	
	public void actionListenerSeleccionarAutomovil(Automovil automovil) {
		placaAutomovilSeleccionado=automovil.getPlaca();
		if(automovil.getAlquilado()) {
			JSFUtil.crearMensajeError("El automóvil ya está alquilado.");
			return;
		}
	}
	public void actionListenerAlquilarSimpleAutomovil() {
		try {
			managerAlquiler.alquilerSimpleAutomovil(placaAutomovilSeleccionado, cedulaClienteSeleccionado);
			listaAutomoviles=managerAlquiler.findAllAutomovil();
			JSFUtil.crearMensajeInfo("El auto ha sido alquilado.");
		}catch(Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
		}
	}
	
	public void actionListenerAgregarDetalleAlquiler() {
		listaDetalleAlquiler.add(managerAlquiler.crearDetalleAlquiler(placaAutomovilSeleccionado, numeroDias));
		total=managerAlquiler.totalDetalleAlquiler(listaDetalleAlquiler);
	}
	
	public void actionListenerGuardarAlquileMultiple() {
		managerAlquiler.alquilerMultimpleAutomovil(listaDetalleAlquiler, cedulaClienteSeleccionado);
		JSFUtil.crearMensajeInfo("Se guardó el alquiler múltiple exitosamente");
		listaDetalleAlquiler=new ArrayList<DTODetalleAlquiler>();
		total=0;
	}

	public List<Automovil> getListaAutomoviles() {
		return listaAutomoviles;
	}

	public void setListaAutomoviles(List<Automovil> listaAutomoviles) {
		this.listaAutomoviles = listaAutomoviles;
	}

	public List<Marca> getListaMarcas() {
		return listaMarcas;
	}

	public void setListaMarcas(List<Marca> listaMarcas) {
		this.listaMarcas = listaMarcas;
	}

	public List<Cliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(List<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public Automovil getAutomovilEdicion() {
		return automovilEdicion;
	}

	public void setAutomovilEdicion(Automovil automovilEdicion) {
		this.automovilEdicion = automovilEdicion;
	}

	public int getIdMarcaEdicion() {
		return idMarcaEdicion;
	}

	public void setIdMarcaEdicion(int idMarcaEdicion) {
		this.idMarcaEdicion = idMarcaEdicion;
	}

	public String getCedulaClienteSeleccionado() {
		return cedulaClienteSeleccionado;
	}

	public void setCedulaClienteSeleccionado(String cedulaClienteSeleccionado) {
		this.cedulaClienteSeleccionado = cedulaClienteSeleccionado;
	}

	public List<DTODetalleAlquiler> getListaDetalleAlquiler() {
		return listaDetalleAlquiler;
	}

	public void setListaDetalleAlquiler(List<DTODetalleAlquiler> listaDetalleAlquiler) {
		this.listaDetalleAlquiler = listaDetalleAlquiler;
	}

	public String getPlacaAutomovilSeleccionado() {
		return placaAutomovilSeleccionado;
	}

	public void setPlacaAutomovilSeleccionado(String placaAutomovilSeleccionado) {
		this.placaAutomovilSeleccionado = placaAutomovilSeleccionado;
	}

	public int getNumeroDias() {
		return numeroDias;
	}

	public void setNumeroDias(int numeroDias) {
		this.numeroDias = numeroDias;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}
	
}
